package com.devcamp.s50.task50a80;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task50a80Application {

	public static void main(String[] args) {
		SpringApplication.run(Task50a80Application.class, args);
	}

}
